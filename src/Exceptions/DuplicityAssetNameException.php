<?php

declare(strict_types=1);

namespace h4kuna\Assets\Exceptions;

use RuntimeException;

final class DuplicityAssetNameException extends RuntimeException {

}
