<?php

declare(strict_types=1);

namespace h4kuna\Assets\Exceptions;

use RuntimeException;

final class FileNotFoundException extends RuntimeException {

}
