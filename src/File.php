<?php

declare(strict_types=1);

namespace h4kuna\Assets;

use Nette\Http\UrlScript;

class File {

    private string $rootFs;

    private UrlScript $url;

    private CacheAssets $cache;

    private string|null $hostUrl = null;

    private string|null $basePath = null;

    public function __construct(string $rootFs, UrlScript $url, CacheAssets $cache) {

        $this->rootFs = $rootFs;
        $this->url = $url;
        $this->cache = $cache;
    }

    public function createUrl(string $file): string {

        if (str_starts_with($file, '//')) {
            $host = $this->getHostUrl() . '/';
            $file = substr($file, 2);
        } else {
            $host = $this->getBasePath();
        }

        return $host . $file . '?' . $this->cache->load($this->rootFs . DIRECTORY_SEPARATOR . $file);
    }

    private function getHostUrl(): string {

        if ($this->hostUrl === null) {
            $this->hostUrl = $this->url->getHostUrl();
        }

        return $this->hostUrl;
    }

    private function getBasePath(): string {

        if ($this->basePath === null) {
            $this->basePath = $this->url->getBasePath();
        }
        return $this->basePath;
    }
}
